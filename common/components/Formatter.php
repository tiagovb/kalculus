<?php

namespace common\components;

use common\components\enum\EnumBoolSimNao;
use common\components\enum\EnumFormaPagamento;
use common\components\enum\EnumNaturezaOperacao;
use common\components\enum\EnumTipoDocumento;
use common\components\enum\EnumTipoPessoa;
use common\helpers\Enum;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @inheritdoc
 *
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Formatter extends \yii\i18n\Formatter
{
    /* @inheritdoc */
    public $dateFormat = 'medium';

    /* @inheritdoc */
    public $decimalSeparator = ',';

    /* @inheritdoc */
    public $thousandSeparator = '.';

    /**
     * @param $str
     * @param $value
     *
     * @return string
     */
    public function getValue($str, $value)
    {
        return ($str === null) ? $this->nullDisplay : $value;
    }

    /**
     * @inheritdoc
     */
    public function asDatetime($value, $format = null)
    {
        if ($format === null) {
            $format = 'dd/MM/Y - HH:mm';
        }

        return parent::asDate($value, $format);
    }

    /**
     * @inheritdoc
     */
    public function asDate($value, $format = null)
    {
        if ($format === null) {
            $format = 'php:d/m/Y';
        }

        return parent::asDate($value, $format);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asSexo($value)
    {
        return ArrayHelper::getValue(Enum::sexo(), $value, $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asSimNao($value)
    {
        return ArrayHelper::getValue(Enum::simNao(), $value, $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asBooleanSimNao($value)
    {
        return ArrayHelper::getValue(Enum::booleanSimNao(), $value, $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asTipoPessoa($value)
    {
        return ArrayHelper::getValue(EnumTipoPessoa::getEnumListLabels(), $value, $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asTipoDocumento($value)
    {
        return ArrayHelper::getValue(EnumTipoDocumento::getEnumListLabels(), $value, $this->nullDisplay);
    }

    /**
     * @inheritDoc
     */
    public function asUrlExterna($value, $options = [])
    {
        $options = array_merge($options, ['target' => '_blank']);

        return parent::asUrl($value, $options);
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asCPF($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        if (preg_match('/^(\d{3})(\d{3})(\d{3})(\d{2})$/', $value, $matches)) {
            return "$matches[1].$matches[2].$matches[3]-$matches[4]";
        }

        return $value;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asCNPJ($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        if (preg_match('/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/', $value, $matches)) {
            return "$matches[1].$matches[2].$matches[3]/$matches[4]-$matches[5]";
        }

        return $value;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asDocumento($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        if (preg_match('/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/', $value)) {
            $return = $this->asCNPJ($value);
        } else {
            $return = $this->asCPF($value);
        }

        return $return;
    }

    /**
     * @param null $value
     *
     * @return mixed|null
     */
    public function asSomenteNumeros($value = null)
    {
        if ($value) {
            return preg_replace('/[^0-9]+/', '', $value);
        }

        return $value;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asTelefone($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }
        $value = preg_replace('/[^0-9]/', '', $value);

        if (preg_match('/^(\d{2})(\d{4})\-(\d{4})$/', $value, $matches)) {
            return "($matches[1]) $matches[2]-$matches[3]";
        }

        if (preg_match('/^(\d{2})(\d{4})(\d{4})$/', $value, $matches)) {
            return "($matches[1]) $matches[2]-$matches[3]";
        }

        if (preg_match('/^(\d{2})(\d{5})\-(\d{4})$/', $value, $matches)) {
            return "($matches[1]) $matches[2]-$matches[3]";
        }

        if (preg_match('/^(\d{2})(\d{5})(\d{4})$/', $value, $matches)) {
            return "($matches[1]) $matches[2]-$matches[3]";
        }

        return $value;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asCEP($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        if (preg_match('/^(\d{5})(\d{3})$/', $value, $matches)) {
            return "$matches[1]-$matches[2]";
        }

        return $value;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asGridAtivo($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $simNao = Enum::ativoInativo();

        if (isset($simNao[$value])) {
            return $value == Enum::BOOLEAN_TRUE ?
                '<span class="label label-success">' . Yii::t('app', 'Ativo') . '</span>' :
                '<span class="label label-danger">' . Yii::t('app', 'Inativo') . '</span>';
        }

        return $value;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asLabelIsVisivel($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $simNao = EnumBoolSimNao::getEnumListLabels();

        if (isset($simNao[$value])) {
            return $value == EnumBoolSimNao::BOOL_SIM ?
                '<span class="label label-success"><i class="icon-eye4"></i></span>'
                : '<span class="label label-danger"><i class="icon-eye5"></i></span>';
        }

        return $value;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asLeadValidacao($value)
    {
        $label = $value ? 'Pendente de Validação' : 'Validado';
        $class = $value ? 'warning' : 'success';

        return Html::tag('span', Yii::t('app', $label), ['class' => 'label label-' . $class]);
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asLabelStatus($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $status = Enum::status();

        if (isset($status[$value])) {
            switch ($value) {
                case Enum::STATUS_ATIVO:
                    return '<span class="label label-success">' . \Yii::t('app', 'Ativo') . '</span>';
                case Enum::STATUS_EXCLUIDO:
                    return '<span class="label label-danger">' . \Yii::t('app', 'Excluído') . '</span>';
                case Enum::STATUS_PENDENTE:
                    return '<span class="label label-warning">' . \Yii::t('app', 'Pendente') . '</span>';
                default:
                    return '<span class="label label-default">' . $status[$value] . '</span>';
            }
        }

        return '<span class="label label-default">' . $value . '</span>';
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asLabelSimNao($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $enum = EnumBoolSimNao::getEnumListLabels();

        if (isset($enum[$value])) {
            if ($value == EnumBoolSimNao::BOOL_SIM) {
                return '<span class="label label-success">' . \Yii::t('app', 'Sim') . '</span>';
            } elseif ($value == EnumBoolSimNao::BOOL_NAO) {
                return '<span class="label label-danger">' . \Yii::t('app', 'Não') . '</span>';
            }
        }

        return '<span class="label label-default">' . $value . '</span>';
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asLabelObrigatorioOpcional($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $keys = EnumBoolSimNao::getEnumListLabels();

        if (isset($keys[$value])) {
            if ($value == EnumBoolSimNao::BOOL_SIM) {
                return '<span class="label label-danger">' . \Yii::t('app', 'Obrigatório') . '</span>';
            } elseif ($value == EnumBoolSimNao::BOOL_NAO) {
                return '<span class="label label-primary">' . \Yii::t('app', 'Opcional') . '</span>';
            }
        }

        return '<span class="label label-default">' . $value . '</span>';
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asAppendHoras($value)
    {
        return $this->getValue($value, $value . ($value == 1 ? ' ' . \Yii::t('app', 'Hora') : ' ' . \Yii::t('app', ' Horas')));
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asAppendMinutos($value)
    {
        return $this->getValue($value, $value . ($value == 1 ? ' ' . \Yii::t('app', 'Minuto') : ' ' . \Yii::t('app', 'Minutos')));
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asAppendDias($value)
    {
        return $this->getValue($value, $value . ($value == 1 ? ' ' . \Yii::t('app', 'Dia') : ' ' . \Yii::t('app', 'Dias')));
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asCor($value)
    {
        return $this->getValue($value, '<div style="height: 20px; width: 20px; background-color: ' . $value . '"></div>');
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asInterfaceCliente($value)
    {
        return ArrayHelper::getValue(Enum::interfaceCliente(), $value, $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asNaturezaOperacao($value)
    {
        return EnumNaturezaOperacao::getEnumLabel($value);
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asFormaPagamento($value)
    {
        return EnumFormaPagamento::getEnumLabel($value);
    }
}
