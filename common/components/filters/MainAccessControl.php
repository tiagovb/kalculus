<?php

namespace common\components\filters;

use yii\filters\AccessControl;

/**
 * Class MainAccessControl
 */
class MainAccessControl extends AccessControl
{
    /**
     * @inheritdoc
     */
    public $except = [
        'user/*',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->rules = [
            [
                // Autenticado pode tudo
                'allow' => true,
                'roles' => ['@'],
            ],
        ];

        parent::init();
    }
}
