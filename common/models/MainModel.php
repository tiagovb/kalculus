<?php

namespace common\models;

use common\models\query\MainQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 *
 * @method MainQuery find() static
 *
 * @property array $dynamicAttributes
 * @property array $relationAttributes
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class MainModel extends ActiveRecord
{
    /* @var string */
    public static $campoDescModel;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->modelName();
    }

    /**
     * @return string
     */
    public static function getCampoDescModel()
    {
        if (static::$campoDescModel === null) {
            trigger_error("Não é possível printar o model " . self::className() . '. Não foi possível definir o campo de descrição do modelo.', E_USER_ERROR);
        }

        return static::$campoDescModel;
    }

    /**
     * @return string
     */
    public static function getSqlCampoDescModel()
    {
        return static::getCampoDescModel();
    }

    /**
     * Retorna a string que descreve o modelo.
     */
    public function modelName()
    {
        $campoDesc = static::getCampoDescModel();

        /** @noinspection PhpVariableVariableInspection */
        return (string)$this->$campoDesc;
    }

    /**
     * Define as condições para listar um model
     *
     * @return $this
     *
     * @internal param null|string $q
     */
    public function lista()
    {
        trigger_error("Não é possível listar o model " . self::className() . '. O método lista() não foi implementado.', E_USER_ERROR);
    }

    /**
     * @param $id
     *
     * @return ActiveQuery|MainModel
     */
    public static function findById($id)
    {
        $query = static::find();

        return $query->andWhere([static::tableName() . '.id' => $id]);
    }

    /**
     * @param string $query O texto a ser procurado
     * @param bool $selecione Se deverá ter Selecione...
     *
     * @return array
     */
    public static function listaDropdown($query = null, $selecione = null)
    {
        $dados = $selecione === null ? [null => 'Selecione...'] : [];

        /** @var MainQuery $class */
        $class = get_called_class();
        $result = $class::find()->lista($query)->createCommand()->queryAll();

        $dados = $result ? $dados + ArrayHelper::map($result, 'id', 'text') : $dados;

        return $dados;
    }

    /**
     * @param $class
     *
     * @return string
     */
    protected function getRelationName($class)
    {
        $myClass = get_called_class();
        $myNS = substr($myClass, 0, strrpos($myClass, '\\'));

        return $myNS . substr($class, strrpos($class, '\\'));
    }
}
