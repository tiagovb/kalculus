<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Condominio;

/**
 * CondominioSearch represents the model behind the search form of `common\models\Condominio`.
 */
class CondominioSearch extends Condominio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['razaoSocial', 'cnpj', 'endLogradouro', 'endNumero', 'endComplemento', 'endBairro', 'endCidade', 'endEstado'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Condominio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'razaoSocial', $this->razaoSocial])
            ->andFilterWhere(['like', 'cnpj', $this->cnpj])
            ->andFilterWhere(['like', 'endLogradouro', $this->endLogradouro])
            ->andFilterWhere(['like', 'endNumero', $this->endNumero])
            ->andFilterWhere(['like', 'endComplemento', $this->endComplemento])
            ->andFilterWhere(['like', 'endBairro', $this->endBairro])
            ->andFilterWhere(['like', 'endCidade', $this->endCidade])
            ->andFilterWhere(['like', 'endEstado', $this->endEstado]);

        return $dataProvider;
    }
}
