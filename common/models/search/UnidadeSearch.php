<?php

namespace common\models\search;

use common\models\Unidade;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UnidadeSearch represents the model behind the search form of `common\models\Unidade`.
 */
class UnidadeSearch extends Unidade
{
    /* @var string */
    public $proprietario;

    /* @var string */
    public $condominio;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'condominioId', 'proprietarioId'], 'integer'],
            [['cliente', 'oferta', 'numero'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Unidade::find();

        $query->joinWith('condominio');
        $query->joinWith('proprietario');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['proprietario'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['nome' => SORT_ASC],
            'desc' => ['nome' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['condominio'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['razaoSocial' => SORT_ASC],
            'desc' => ['razaoSocial' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            '{{unidade}}.id' => $this->id,
            'condominioId' => $this->condominioId,
            'proprietarioId' => $this->proprietarioId,
        ]);

        $query
            ->andFilterWhere(['like', 'numero', $this->numero])
            ->andFilterWhere(['like', 'nome', $this->proprietario])
            ->andFilterWhere(['like', 'razaoSocial', $this->condominio]);

        return $dataProvider;
    }
}
