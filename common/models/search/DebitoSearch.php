<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Debito;

/**
 * DebitoSearch represents the model behind the search form of `common\models\Debito`.
 */
class DebitoSearch extends Debito
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'unidadeId', 'atualizacaoId'], 'integer'],
            [['vencimento', 'dataCalculo', 'descricao'], 'safe'],
            [['valorOriginal', 'totalDebito', 'valorMulta', 'valorAtualizado', 'totalJuros'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Debito::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vencimento' => $this->vencimento,
            'valorOriginal' => $this->valorOriginal,
            'totalDebito' => $this->totalDebito,
            'dataCalculo' => $this->dataCalculo,
            'valorMulta' => $this->valorMulta,
            'valorAtualizado' => $this->valorAtualizado,
            'totalJuros' => $this->totalJuros,
            'unidadeId' => $this->unidadeId,
            'atualizacaoId' => $this->atualizacaoId,
        ]);

        $query->andFilterWhere(['like', 'descricao', $this->descricao]);

        return $dataProvider;
    }
}
