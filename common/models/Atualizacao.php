<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "atualizacao".
 *
 * @property integer $id
 * @property double $fator
 * @property string $dtPublicacao
 * @property integer $mesReferencia
 * @property integer $anoReferencia
 * @property string $hashTabela
 *
 * @property Debito[] $debitos
 */
class Atualizacao extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'atualizacao';
    }

    public function modelName()
    {
        return $this->fator;
    }


    /**
     * @param $vencimento
     *
     * @return query\MainQuery
     */
    public static function findByVencimento($vencimento)
    {
        $mes = date("m", strtotime($vencimento));
        $ano = date("Y", strtotime($vencimento));

        return static::find()->andWhere([
            'AND',
            ['mesReferencia' => $mes],
            ['anoReferencia' => $ano],
        ])->orderBy(['dtPublicacao' => SORT_DESC, 'id' => SORT_DESC])->limit(1);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fator'], 'number'],
            [['hashTabela'], 'string'],
            [['dtPublicacao'], 'safe'],
            [['mesReferencia', 'anoReferencia'], 'integer'],
            [['dtPublicacao', 'mesReferencia', 'anoReferencia'], 'unique', 'targetAttribute' => ['dtPublicacao', 'mesReferencia', 'anoReferencia'], 'message' => 'The combination of Dt Publicacao, Mes Referencia and Ano Referencia has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fator' => 'Fator',
            'dtPublicacao' => 'Dt Publicacao',
            'mesReferencia' => 'Mes Referencia',
            'anoReferencia' => 'Ano Referencia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebitos()
    {
        return $this->hasMany(Debito::className(), ['atualizacaoId' => 'id']);
    }

    public function showPublicacao()
    {
        return Yii::$app->formatter->asDate($this->dtPublicacao, 'php:d/m/Y');
    }
}
