<?php

namespace common\models;

use yii\base\InvalidParamException;
use yii\db\Expression;

/**
 * This is the model class for table "debito".
 *
 * @property integer $id
 * @property string $vencimento
 * @property string $valorOriginal
 * @property string $totalDebito
 * @property string $dataCalculo
 * @property string $valorMulta
 * @property string $valorAtualizado
 * @property string $totalJuros
 * @property string $descricao
 * @property integer $unidadeId
 * @property integer $atualizacaoId
 *
 * @property Unidade $unidade
 * @property Atualizacao $atualizacao
 */
class Debito extends MainModel
{
    const PERCENT_MULTA = 0.02;
    const PERCENT_JUROS = 0.01;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'debito';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descricao', 'valorOriginal', 'vencimento'], 'required'],
            [['id', 'unidadeId', 'atualizacaoId'], 'integer'],
            [['dataCalculo'], 'safe'],
            [['totalDebito', 'valorMulta', 'valorAtualizado', 'totalJuros'], 'number'],
            [['descricao'], 'string', 'max' => 100],
            [
                ['unidadeId'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Unidade::className(),
                'targetAttribute' => ['unidadeId' => 'id']
            ],
            [
                ['atualizacaoId'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Atualizacao::className(),
                'targetAttribute' => ['atualizacaoId' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vencimento' => 'Vencimento',
            'valorOriginal' => 'Valor',
            'totalDebito' => 'Total',
            'dataCalculo' => 'Data Calculo',
            'valorMulta' => 'Multa',
            'valorAtualizado' => 'Valor Atualizado',
            'totalJuros' => 'Total Juros',
            'descricao' => 'Descricao',
            'unidadeId' => 'Unidade',
            'atualizacaoId' => 'Atualizacao',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnidade()
    {
        return $this->hasOne(Unidade::className(), ['id' => 'unidadeId'])->inverseOf('debitos');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtualizacao()
    {
        return Atualizacao::findByVencimento($this->vencimento);
    }

    public function calcular()
    {
        if (!empty($this->vencimento) && $this->valorOriginal) {
            $mesesVencidos = $this->getMesesVencidos();
            $this->atualizacaoId = $this->atualizacao->id;
            $this->dataCalculo = new Expression('NOW()');
            $this->valorMulta = (float)$this->valorOriginal * self::PERCENT_MULTA;
            $this->valorAtualizado = ((float)$this->valorOriginal + (float)$this->valorMulta) * (float)$this->atualizacao->fator;
            $this->totalJuros = (float)$this->valorAtualizado * (float)$mesesVencidos * (float)self::PERCENT_JUROS;
            $this->totalDebito = (float)$this->valorAtualizado + (float)$this->totalJuros;
            return $this->save(false);
        }

        return false;
    }

    public function getMesesVencidos()
    {
        $vencimento = $this->vencimento;

        if ($vencimento) {

            $mesVenc = date('m', strtotime($vencimento));
            $anoVenc = date('Y', strtotime($vencimento));
            $anoAgora = date('Y');
            $mesAgora = date('m');
            $anosVencidos = $anoAgora - $anoVenc;
            return ($mesAgora - $mesVenc + 12 * $anosVencidos);
        }

        throw new InvalidParamException('Vencimento não informado!');
    }
}
