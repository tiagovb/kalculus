<?php

namespace common\models;

/**
 * This is the model class for table "condominio".
 *
 * @property integer $id
 * @property string $razaoSocial
 * @property string $cnpj
 * @property string $endLogradouro
 * @property string $endNumero
 * @property string $endComplemento
 * @property string $endBairro
 * @property string $endCidade
 * @property string $endEstado
 *
 * @property Unidade[] $unidades
 */
class Condominio extends MainModel
{
    /**
     * @inheritdoc
     *
     * @return query\CondominioQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new query\CondominioQuery(get_called_class());
    }

    /* @inheritdoc */
    public function modelName()
    {
        return $this->razaoSocial;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'condominio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['razaoSocial', 'endBairro', 'endCidade'], 'string', 'max' => 100],
            [['cnpj', 'endEstado'], 'string', 'max' => 20],
            [['endLogradouro'], 'string', 'max' => 200],
            [['endNumero', 'endComplemento'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'razaoSocial' => 'Razao Social',
            'cnpj' => 'Cnpj',
            'endLogradouro' => 'End Logradouro',
            'endNumero' => 'End Numero',
            'endComplemento' => 'End Complemento',
            'endBairro' => 'End Bairro',
            'endCidade' => 'End Cidade',
            'endEstado' => 'End Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnidades()
    {
        return $this->hasMany(Unidade::className(), ['condominioId' => 'id'])->inverseOf('condominio');
    }
}
