<?php

namespace common\models;

/**
 * This is the model class for table "proprietario".
 *
 * @property integer $id
 * @property string $nome
 * @property string $cpf
 * @property string $telefone
 * @property string $email
 *
 * @property Unidade[] $unidades
 */
class Proprietario extends MainModel
{
    /**
     * @inheritdoc
     *
     * @return query\ProprietarioQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new query\ProprietarioQuery(get_called_class());
    }

    /* @inheritdoc */
    public function modelName()
    {
        return $this->nome;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proprietario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome', 'email'], 'string', 'max' => 100],
            [['cpf', 'telefone'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'cpf' => 'Cpf',
            'telefone' => 'Telefone',
            'email' => 'E-mail',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnidades()
    {
        return $this->hasMany(Unidade::className(), ['proprietarioId' => 'id'])->inverseOf('proprietario');
    }
}
