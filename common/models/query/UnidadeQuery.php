<?php

namespace common\models\query;

use common\models\Unidade;

/**
 * This is the ActiveQuery class for [[\common\models\Unidade]].
 *
 * @method Unidade[]|array all($db = null)
 * @method Unidade|array|null one($db = null)
 *
 * @see \common\models\Unidade
 */
class UnidadeQuery extends MainQuery
{
    /**
     * @inheritdoc
     */
    public function lista($query = null)
    {
        $this->select = ['{{%unidade}}.id', 'text' => "CONCAT({{%unidade}}.numero, ' - ', {{%condominio}}.razaoSocial)", 'razaoSocial'];
        $this->orderBy = ['numero' => SORT_ASC];
        $this->joinWith('condominio', false);

        if ($query) {
            $this->where(['like', 'numero', "{$query}%", false]);
        }

        return $this;
    }
}
