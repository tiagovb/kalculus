<?php

namespace common\models\query;

use common\models\MainModel;
use yii\db\ActiveQuery;

/**
 * Class MainQuery
 *
 * @method $this find() static
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class MainQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     *
     * @return \common\models\MainModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     *
     * @return \common\models\MainModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param string|null $query
     *
     * @return $this
     */
    public function lista($query = null)
    {
        /** @var MainModel $modelClass */
        $modelClass = $this->modelClass;
        $campoText = $modelClass::getSqlCampoDescModel();
        $this->select = ['id', 'text' => $campoText];
        $this->orderBy = [$campoText => SORT_ASC];

        $this->andFilterWhere(['like', $campoText, "%{$query}%", false]);

        return $this;
    }
}
