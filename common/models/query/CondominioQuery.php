<?php

namespace common\models\query;

use common\models\Condominio;

/**
 * This is the ActiveQuery class for [[\common\models\Condominio]].
 *
 * @method Condominio[]|array all($db = null)
 * @method Condominio|array|null one($db = null)
 *
 * @see \common\models\Condominio
 */
class CondominioQuery extends MainQuery
{
    /**
     * @inheritdoc
     */
    public function lista($query = null)
    {
        $this->select = ['{{%condominio}}.id', 'text' => "{{%condominio}}.razaoSocial", 'razaoSocial'];
        $this->orderBy = ['razaoSocial' => SORT_ASC];

        if ($query) {
            $this->where(['like', 'razaoSocial', "{$query}%", false]);
        }

        return $this;
    }
}
