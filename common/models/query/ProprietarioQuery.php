<?php

namespace common\models\query;

use common\models\Proprietario;

/**
 * This is the ActiveQuery class for [[\common\models\Proprietario]].
 *
 * @method Proprietario[]|array all($db = null)
 * @method Proprietario|array|null one($db = null)
 *
 * @see \common\models\Proprietario
 */
class ProprietarioQuery extends MainQuery
{
    /**
     * @inheritdoc
     */
    public function lista($query = null)
    {
        $this->select = ['{{%proprietario}}.id', 'text' => "{{%proprietario}}.nome", 'nome'];
        $this->orderBy = ['nome' => SORT_ASC];

        if ($query) {
            $this->where(['like', 'nome', "{$query}%", false]);
        }

        return $this;
    }
}
