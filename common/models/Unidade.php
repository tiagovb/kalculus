<?php

namespace common\models;

/**
 * This is the model class for table "unidade".
 *
 * @property integer $id
 * @property string $numero
 * @property integer $condominioId
 * @property integer $proprietarioId
 *
 * @property Debito[] $debitos
 * @property Condominio $condominio
 * @property Proprietario $proprietario
 */
class Unidade extends MainModel
{
    /**
     * @inheritdoc
     *
     * @return query\UnidadeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new query\UnidadeQuery(get_called_class());
    }

    /* @inheritdoc */
    public function modelName()
    {
        return $this->numero . ' - ' . $this->condominio->razaoSocial;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unidade';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'condominioId', 'proprietarioId'], 'integer'],
            [['condominioId'], 'required'],
            [['numero'], 'string', 'max' => 10],
            [['totalDebitos'], 'number'],
            [['condominioId'], 'exist', 'skipOnError' => true, 'targetClass' => Condominio::className(), 'targetAttribute' => ['condominioId' => 'id']],
            [['proprietarioId'], 'exist', 'skipOnError' => true, 'targetClass' => Proprietario::className(), 'targetAttribute' => ['proprietarioId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero' => 'Número',
            'condominioId' => 'Condomínio',
            'proprietarioId' => 'Proprietário',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebitos()
    {
        return $this->hasMany(Debito::className(), ['unidadeId' => 'id'])->inverseOf('unidade');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCondominio()
    {
        return $this->hasOne(Condominio::className(), ['id' => 'condominioId'])->inverseOf('unidades');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProprietario()
    {
        return $this->hasOne(Proprietario::className(), ['id' => 'proprietarioId'])->inverseOf('unidades');
    }

    public function getTotalDebitos()
    {
        return Debito::find()->andWhere(['unidadeId' => $this->id])->sum('totalDebito') ?: 0;
    }

    public function getHonorarios()
    {
        return round($this->getTotalDebitos() * 0.1, 2);
    }

    public function atualizarDebitos()
    {
        $debitos = $this->debitos;
        foreach ($debitos as $debito) {
            $debito->calcular();
        }
    }
}
