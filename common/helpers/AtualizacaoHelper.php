<?php


namespace common\helpers;


use common\models\Atualizacao;
use Goutte\Client;
use Spatie\PdfToText\Pdf;
use talma\sendgrid\Message;
use Yii;
use yii\base\Exception;

class AtualizacaoHelper
{
    /**
     * @return string
     */
    public static function getUrlPdf()
    {
        $client = new Client();
        $crawler = $client->request('GET', 'http://www.tjmg.jus.br/portal/processos/fator-de-atualizacao-monetaria/');

        $rawLink = $crawler->filter('.DownloadDocument a')->first()->attr('href');
        return str_replace(" ", "%20", str_replace("../../../", "http://www.tjmg.jus.br/", $rawLink));
    }
///**
//     * @return string
//     */
//    public static function getUrlPdf()
//    {
//        $client = new Client();
//        $crawler = $client->request('GET', 'http://www.tjmg.jus.br/portal/processos/fator-de-atualizacao-monetaria/');
//
//        $rawLink = $crawler->filter('.DownloadDocument a')->first()->attr('href');
//        return str_replace(" ", "%20", str_replace("../../../", "http://www.tjmg.jus.br/", $rawLink));
//    }

    public static function sendEmail($dtPublicacao)
    {
        $dtPublicacao = Yii::$app->formatter->asDate($dtPublicacao);

        /** @var Message $message */
        $message = new Message();
        $message->addCategories(['Aviso Nova Atualizacao']);
        $message->setTextBody("Nova tabela de fatores de atualização disponibilizada em {$dtPublicacao}");
        $message->setHtmlBody("<html><p>Nova tabela de fatores de atualização disponibilizada em {$dtPublicacao}</p></html>");
        $message->setFrom(['contato@tiagoviana.com' => 'Atualização TJMG']);
        $message->setSubject("Novos Fatores de Atualização");
        $message->setTo([
            'tiagovianabatista@gmail.com' => 'Tiago Viana',
            'dudalimpi@yahoo.com.br' => 'Priscila Limpi',
        ]);
        $message->send();
    }

    public static function baixarTabela($url)
    {
//        $url = self::getUrlPdf();

        return self::getFromPdf($url);
    }

    private static function getFromPdf($pdfUrl)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $pdfPath = \Yii::$app->runtimePath . '/temp_pdf_tabela';
            file_put_contents($pdfPath, fopen($pdfUrl, 'r'));
            $text = str_replace("\n", '', Pdf::getText($pdfPath));

            if (empty($text)) {
                $transaction->commit();
                return false;
            }

            $hashTabela = hash('sha512', $text, false);

            $dtPublicacao = date('Y-m-d');
            $anoAtual = date('Y');

            foreach (range(2000, $anoAtual) as $anoReferencia) {
                $matches = [];
                preg_match_all("/($anoReferencia)((\d{1}[,]\d{7})+)($anoReferencia)/", $text, $matches, PREG_PATTERN_ORDER);
                $str = $matches[2][0] ?? '';
                $re = '/(\d{1}[,]\d{7})/';
                preg_match_all($re, $str, $matches, PREG_PATTERN_ORDER);
                $fatores = $matches[0] ?? null;

                foreach ($fatores as $key => $fatorAtual) {
                    $mes = $key + 1;
                    (new Atualizacao(['mesReferencia' => $mes, 'fator' => str_replace(',', '.', $fatorAtual), 'anoReferencia' => $anoReferencia, 'dtPublicacao' => $dtPublicacao, 'hashTabela' => $hashTabela]))->save(false);
                }
            }

            self::sendEmail($dtPublicacao);

            $transaction->commit();
        } catch (\Throwable $throwable) {
            $transaction->rollBack();

            return false;
        }

        return true;

    }

    public static function getMesAnoTabela($textoPdf)
    {
        preg_match('/Publicação: (\d{2}\/\d{2}\/\d{4})/', $textoPdf, $matches);
        $dtPublicacao = end($matches);

        return self::toSqlDate($dtPublicacao);
    }

    /**
     * @param $valor
     *
     * @return string
     */
    public static function toSqlDate($valor)
    {
        if (strpos($valor, '/') !== false) {
            $valor = str_replace('/', '-', $valor);
        }

        return Yii::$app->formatter->asDate($valor, 'php:Y-m-d');
    }
}
