<?php
return [
    'language' => 'pt-BR',
    'sourceLanguage' => 'pt-BR',
    'timeZone' => 'America/Sao_Paulo',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'formatter' => [
            'class' => 'common\components\Formatter',
            'defaultTimeZone' => 'America/Sao_Paulo',
            'timeZone' => 'America/Sao_Paulo',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'urlPrefix' => 'u',
            'urlRules' => [
                '<id:\d+>' => 'profile/show',
                '<action:(login|logout)>' => 'security/<action>',
                'cadastro' => 'registration/register',
                'reenviar' => 'registration/resend',
                'confirmar/<id:\d+>/<token:\w+>' => 'registration/confirm',
                'esqueci' => 'recovery/request',
                'r/<id:\d+>/<token:\w+>' => 'recovery/reset',
                'config/<action:\w+>' => 'settings/<action>',
            ],
            // you will configure your module inside this file
            // or if need different configuration for frontend and backend you may
            // configure in needed configs
        ],
    ],
];
