<?php

namespace console\controllers;

use common\helpers\AtualizacaoHelper;
use talma\sendgrid\Message;
use Yii;
use yii\console\Controller;
use yii\helpers\Json;

/**
 * Class TesteController
 */
class TesteController extends Controller
{
    public function actionIndex()
    {
        AtualizacaoHelper::baixarTabela();
    }

    public function actionEnv()
    {
        echo 'Enviroment: ' . YII_ENV . "\n";
    }

    public function actionEscalaLimpeza()
    {
        $escalaFile = Yii::$app->basePath . '/../escala-limpeza/escala.json';

        $escala = Json::decode(file_get_contents($escalaFile));

        $ultimaEscala = end($escala);

        reset($escala);

        $novaEscala =
            [
                'dias' => date('d/m') . '-' . date('d/m', strtotime('+3 day')),
                'area' => $ultimaEscala['cozinha'],
                'banheiro' => $ultimaEscala['area'],
                'cozinha' => $ultimaEscala['banheiro'],
            ];

        $escala[] = $novaEscala;

        $emails = [
            ['tiagovianabatista@gmail.com' => 'Tiago Viana'],
            ['rodrigo.viana@engenharia.ufjf.br' => 'Rodrigo Viana'],
            ['ricardo-miranda@live.de' => 'Ricardo Miranda'],
        ];

        foreach ($emails as $email) {
            /** @var Message $message */
            $message = new Message();
            $message->addCategories(['Escala limpeza']);
            $message->setTextBody("Escala da limpeza entre os dias {$novaEscala['dias']}: Área:{$novaEscala['area']}, Banheiro:{$novaEscala['banheiro']}, Cozinha:{$novaEscala['cozinha']}");
            $htmlBody = "<h3>Escala de limpeza entre os dias {$novaEscala['dias']}</h3><br>
                         <b>Área:</b> {$novaEscala['area']}<br>
                         <b>Banheiro:</b> {$novaEscala['banheiro']}<br>
                         <b>Cozinha:</b> {$novaEscala['cozinha']}<br>";
            $message->setHtmlBody("<html><p>{$htmlBody}</p></html>");
            $message->setFrom(['contato@tiagoviana.com' => "Escala de Limpeza"]);
            $message->setSubject("Escala de Limpeza {$novaEscala['dias']}");
            $message->setTo($email);
            $message->send();
        }


        file_put_contents($escalaFile, Json::encode($escala, JSON_PRETTY_PRINT));
    }
}
