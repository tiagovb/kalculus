<?php

namespace console\controllers;

use common\helpers\AtualizacaoHelper;
use yii\console\Controller;

/**
 * Class CronController
 */
class CronController extends Controller
{
    public function actionBaixarTabela()
    {
        AtualizacaoHelper::baixarTabela();
    }
}
