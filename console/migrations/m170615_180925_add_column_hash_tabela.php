<?php

use yii\db\Migration;

class m170615_180925_add_column_hash_tabela extends Migration
{
    public function up()
    {
        $this->addColumn('atualizacao', 'hashTabela', $this->string());
    }

    public function down()
    {
        echo "m170615_180925_add_column_hash_tabela cannot be reverted.\n";

        return false;
    }
}
