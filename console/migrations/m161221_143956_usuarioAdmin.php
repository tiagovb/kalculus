<?php

use dektrium\user\models\User;
use yii\db\Migration;

class m161221_143956_usuarioAdmin extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if (!YII_ENV_DEV) {
            return true;
        }

        $user = new User();
        $user->setAttributes([
            'username' => 'admin',
            'email' => 'tiagovianabatista@gmail.com'
        ]);

        $user->password = '32165421';

        $user->create();

        $array = [
            $keys = [
                'id' => '',
                'vencimento' => '',
                'valor' => '',
                'atualizacao' => '',
                'totalDebito' => '',
                'condominio' => '',
                'unidade' => '',
                'dataCalculo' => '',
                'multa' => '',
                'valorAtualizado' => '',
                'totalJuros' => '',
                'descricao' => ''
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        User::deleteAll();
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
