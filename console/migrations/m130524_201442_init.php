<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        if (!YII_ENV_DEV) {
            return true;
        }

        $scriptBanco = <<<SQL

-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: kalculus
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atualizacao`
--

DROP TABLE IF EXISTS `atualizacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atualizacao` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `fator` DOUBLE DEFAULT NULL,
  `dtPublicacao` DATE DEFAULT NULL,
  `mesReferencia` INT(11) DEFAULT NULL,
  `anoReferencia` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_referenciaUnica` (`dtPublicacao`,`mesReferencia`,`anoReferencia`)
) ENGINE=InnoDB AUTO_INCREMENT=3745 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

-- Table structure for table `condominio`
--

DROP TABLE IF EXISTS `condominio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `condominio` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `razaoSocial` VARCHAR(100) DEFAULT NULL,
  `cnpj` VARCHAR(20) DEFAULT NULL,
  `endLogradouro` VARCHAR(200) DEFAULT NULL,
  `endNumero` VARCHAR(10) DEFAULT NULL,
  `endComplemento` VARCHAR(10) DEFAULT NULL,
  `endBairro` VARCHAR(100) DEFAULT NULL,
  `endCidade` VARCHAR(100) DEFAULT NULL,
  `endEstado` VARCHAR(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `debito`
--

DROP TABLE IF EXISTS `debito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debito` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `vencimento` DATE DEFAULT NULL,
  `valorOriginal` DECIMAL(10,2) UNSIGNED NOT NULL,
  `totalDebito` DECIMAL(10,2) UNSIGNED DEFAULT NULL,
  `dataCalculo` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `valorMulta` DECIMAL(10,2) DEFAULT NULL,
  `valorAtualizado` DECIMAL(10,2) DEFAULT NULL,
  `totalJuros` DECIMAL(10,2) DEFAULT NULL,
  `descricao` VARCHAR(100) DEFAULT NULL,
  `unidadeId` BIGINT(20) DEFAULT NULL,
  `atualizacaoId` BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unidadeId` (`unidadeId`),
  KEY `atualizacaoId` (`atualizacaoId`),
  CONSTRAINT `debito_ibfk_1` FOREIGN KEY (`unidadeId`) REFERENCES `unidade` (`id`),
  CONSTRAINT `debito_ibfk_2` FOREIGN KEY (`atualizacaoId`) REFERENCES `atualizacao` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `proprietario`
--

DROP TABLE IF EXISTS `proprietario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proprietario` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) DEFAULT NULL,
  `cpf` VARCHAR(20) DEFAULT NULL,
  `telefone` VARCHAR(20) DEFAULT NULL,
  `email` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unidade`
--

DROP TABLE IF EXISTS `unidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidade` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `numero` VARCHAR(10) DEFAULT NULL,
  `condominioId` BIGINT(20) DEFAULT NULL,
  `proprietarioId` BIGINT(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `condominioId` (`condominioId`),
  KEY `proprietarioId` (`proprietarioId`),
  CONSTRAINT `unidade_ibfk_1` FOREIGN KEY (`condominioId`) REFERENCES `condominio` (`id`),
  CONSTRAINT `unidade_ibfk_2` FOREIGN KEY (`proprietarioId`) REFERENCES `proprietario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-21 23:52:37

SQL;
        $this->db->createCommand($scriptBanco)->execute();

        return true;
    }

    public function down()
    {
        return false;
    }
}
