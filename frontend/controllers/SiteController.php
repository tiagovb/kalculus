<?php
namespace frontend\controllers;

/**
 * Site controller
 */
class SiteController extends MainController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
