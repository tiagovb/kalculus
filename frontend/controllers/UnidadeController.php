<?php

namespace frontend\controllers;

use common\components\Flash;
use common\models\Condominio;
use common\models\Debito;
use common\models\Proprietario;
use common\models\search\DebitoSearch;
use common\models\search\UnidadeSearch;
use common\models\Unidade;
use Yii;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;

/**
 * UnidadeController implements the CRUD actions for Unidade model.
 */
class UnidadeController extends MainController
{
    /**
     * Lists all Unidade models.
     * @return mixed
     */
    public function actionIndex()
    {
//        include_once '../../vendor/phpoffice/phpword/samples/Sample_09_Tables.php';

        $searchModel = new UnidadeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Unidade model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $debitosSearchModel = new DebitoSearch(['unidadeId' => $id]);
        $debitosProvider = $debitosSearchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'debitosSearchModel' => $debitosSearchModel,
            'debitosProvider' => $debitosProvider,
        ]);
    }

    /**
     * Displays a single Unidade model.
     * @param integer $id
     * @return mixed
     */
    public function actionAtualizarDebitos($id)
    {
        $unidade = $this->findModel($id);
        $unidade->atualizarDebitos();

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Displays a single Unidade model.
     * @param integer $id
     * @return mixed
     */
    public function actionMostrarTabela($id)
    {
        $unidade = $this->findModel($id);
        $this->render('tabelaScript', [
            'unidade' => $unidade,
            'debitos' => $unidade->debitos,
        ]);

        $file = '../views/unidade/tabelas/tabela.docx';
        if (file_exists($file)) {
            Yii::$app->response->sendFile($file);
        }

        return $this->render('tabela-erro');

    }

    /**
     * Creates a new Unidade model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Unidade();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Unidade cadastrada com sucesso!');

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Unidade model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNovoCondominio()
    {
        $condominio = new Condominio();

        if ($condominio->load(Yii::$app->request->post()) && $condominio->save()) {
            Flash::success('Condomínio cadastrado com sucesso!');

            return $this->redirect(['index']);
        } else {
            Flash::error('Não foi possível cadastrar o condomínio');
            return $this->redirect('.');
        }
    }

    /**
     * Creates a new Unidade model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNovoProprietario()
    {
        $proprietario = new Proprietario();

        if ($proprietario->load(Yii::$app->request->post()) && $proprietario->save()) {
            Flash::success('Proprietário cadastrado com sucesso!');

            return $this->redirect(['index']);
        } else {
            Flash::error('Não foi possível cadastrar o Proprietário');
            return $this->redirect('.');
        }
    }

    /**
     * @return mixed
     */
    public function actionNovoDebito()
    {
        $id = Yii::$app->request->get('id');
//        $id = ArrayHelper::getValue(Yii::$app->request->get('id'), 'id', false);

        if (!$id) {
            throw new InvalidParamException('Id da unidade não informado!');
        }

        $post = Yii::$app->request->post();
        $debito = new Debito(['unidadeId' => $id]);

        if ($post) {
            $debito->load($post);
            if ($debito->calcular()) {
                $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('_formDebito', [
            'debito' => $debito,
        ]);
    }

    /**
     * Updates an existing Unidade model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Unidade model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionLimparDebitos($id)
    {
        $unidade = $this->findModel($id);

        foreach ($unidade->debitos as $debito) {
            $debito->delete();
        }

        Flash::success('Débitos excluídos com sucesso!');
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Finds the Unidade model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Unidade the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Unidade::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
