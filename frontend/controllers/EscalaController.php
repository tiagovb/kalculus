<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Json;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class EscalaController extends Controller
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $escalaFile = Yii::$app->basePath . '/../escala-limpeza/escala.json';

        $escala = Json::decode(file_get_contents($escalaFile));

        Yii::$app->response->formatters = [
            Response::FORMAT_JSON => [
                'class' => 'yii\web\JsonResponseFormatter',
                'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                // ...
            ]
        ];

        Yii::$app->response->format = Response::FORMAT_JSON;


        return $escala;
    }
}
