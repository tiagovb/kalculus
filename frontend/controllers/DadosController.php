<?php

namespace frontend\controllers;

use common\models\MainModel;
use common\models\query\MainQuery;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Response;

/**
 * Class DadosController
 */
class DadosController extends MainController
{
    /**
     * @var MainQuery
     */
    private $_query;

    /**
     * @var int Página atual
     */
    private $_page;

    /**
     * @var int Número de itens por página
     */
    private $_perPage = 20;

    /**
     * @var array A lista de objetos disponíveis
     */
    public $classes = [
        'unidade' => 'common\models\Unidade',
        'condominio' => 'common\models\Condominio',
        'proprietario' => 'common\models\Proprietario',
    ];

    /**
     * @param $o
     * @param null $id
     * @param null $q
     * @param null $page
     *
     * @return array
     *
     * @throws BadRequestHttpException
     * @throws \yii\base\InvalidConfigException
     *
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function actionPaginados($o, $id = null, $q = null, $page = null)
    {
        /** @var MainModel $model */

        $className = $this->getClassName($o);
        $model = Yii::createObject($className);

        if (!empty($id)) {
            $this->_query = $model::findById(intval($id))->lista();
        } else {
            $this->_query = $model::find()->lista($q);
            $this->_page = ($page ? intval($page) : 1);
        }

        $dados = $this->gerarJson();

        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        return $dados;
    }

    /**
     * @param string $o O objeto que define a classe do Model que será instanciado
     * @param string $q O texto que será usado como filtro
     *
     * @return array Array com os dados retornados, no formato [id => texto]
     *
     * @throws BadRequestHttpException
     * @throws \yii\base\InvalidConfigException
     *
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function actionLista($o, $q = null)
    {
        $className = $this->getClassName($o);
        $model = Yii::createObject($className);
        $dados = $model::listaDropdown($q);

        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        return $dados;
    }

    /**
     * @param string $obj Nome do objeto a ser usado na busca
     *
     * @return MainModel A classe do objeto solicitado
     *
     * @throws BadRequestHttpException
     */
    public function getClassName($obj)
    {
        if (isset($this->classes[$obj])) {
            return $this->classes[$obj];
        }
        throw new BadRequestHttpException('Objeto desconhecido');
    }

    /**
     * @return array
     */
    public function gerarJson()
    {
        $total = $this->_query->count();

        $json = [
            'total' => (int)$total,
            'page' => $this->_page,
            'pages' => ceil($total / $this->_perPage),
            'results' => [],
        ];

        if ($total > 0) {
            // Há mais que 1 página, então será necessário limitar e paginar
            if ($total > $this->_perPage) {
                // Verificar se existe uma página após a página atual
                $json['pagination']['more'] = ($total > ($this->_perPage * $this->_page));

                $this->_query->limit($this->_perPage)->offset(($this->_page - 1) * $this->_perPage);
            }
            $json['results'] = $this->_query->createCommand()->queryAll();
        }

        return $json;
    }
}
