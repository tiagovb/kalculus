<?php
use common\models\Condominio;
use common\models\Proprietario;
use common\models\Unidade;
use common\widgets\Select2;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;

Modal::begin([
    'header' => '<span id="nova-unidade-modal-title">Nova Unidade</span>',
    'id' => 'nova-unidade-modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]); ?>
    <div id="nova-unidade-modal-content">
        <div class="unidade-form">

            <?php $unidade = new Unidade();
            $form = ActiveForm::begin(['action' => '/unidade/create']); ?>

            <?= $form->field($unidade, 'numero')->textInput(['maxlength' => true]) ?>

            <?= $form->field($unidade, 'condominioId')->widget(Select2::className(), [
                'model' => $unidade,
                'initValueText' => Condominio::findOne($unidade->condominioId),
                'attribute' => 'condominioId',
                'objeto' => 'condominio',
            ]) ?>

            <?= $form->field($unidade, 'proprietarioId')->widget(Select2::className(), [
                'model' => $unidade,
                'initValueText' => Proprietario::findOne($unidade->proprietarioId),
                'attribute' => 'proprietarioId',
                'objeto' => 'proprietario',
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Cadastrar', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
<?php Modal::end(); ?>