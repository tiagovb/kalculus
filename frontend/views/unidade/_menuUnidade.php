<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Unidade */


?>
<nav class="navbar navbar-default second-navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <span class="navbar-brand">
                <?= Html::a(Yii::t('app', 'Novo Débito'), ['novo-debito', 'id' => $model->id]) ?> ?>
            </span>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li><?= Html::a(Yii::t('app', 'Atualizar Débitos'), ['atualizar-debitos', 'id' => $model->id]) ?></li>
            </ul>
        </div>
    </div>
</nav>