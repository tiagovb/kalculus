<?php

/* @var $debitos common\models\Debito[] */
/* @var $unidade common\models\Unidade */

use PhpOffice\PhpWord\Settings;

date_default_timezone_set('UTC');
error_reporting(E_ALL);
define('CLI', (PHP_SAPI == 'cli') ? true : false);
define('EOL', CLI ? PHP_EOL : '<br />');
define('SCRIPT_FILENAME', basename($_SERVER['SCRIPT_FILENAME'], '.php'));
define('IS_INDEX', SCRIPT_FILENAME == 'index');

Settings::loadConfig();

// Set writers
$writers = array('Word2007' => 'docx', 'ODText' => 'odt', 'RTF' => 'rtf', 'HTML' => 'html', 'PDF' => 'pdf');

// Set PDF renderer
if (null === Settings::getPdfRendererPath()) {
    $writers['PDF'] = null;
}

// Turn output escaping on
Settings::setOutputEscapingEnabled(true);

// Return to the caller script when runs by CLI
if (CLI) {
    return;
}

// Set titles and names
$pageHeading = str_replace('_', ' ', SCRIPT_FILENAME);
$pageTitle = IS_INDEX ? 'Welcome to ' : "{$pageHeading} - ";
$pageTitle .= 'PHPWord';
$pageHeading = IS_INDEX ? '' : "<h1>{$pageHeading}</h1>";

// Populate samples
$files = '';
if ($handle = opendir('.')) {
    while (false !== ($file = readdir($handle))) {
        if (preg_match('/^Sample_\d+_/', $file)) {
            $name = str_replace('_', ' ', preg_replace('/(Sample_|\.php)/', '', $file));
            $files .= "<li><a href='{$file}'>{$name}</a></li>";
        }
    }
    closedir($handle);
}

/**
 * Write documents
 *
 * @param \PhpOffice\PhpWord\PhpWord $phpWord
 * @param string $filename
 * @param array $writers
 *
 * @return string
 */
function write($phpWord, $filename, $writers)
{
    $result = '';

    // Write documents
    foreach ($writers as $format => $extension) {
        $result .= date('H:i:s') . " Write to {$format} format";
        if (null !== $extension) {
            $targetFile = __DIR__ . "/results/{$filename}.{$extension}";
            $phpWord->save($targetFile, $format);
        } else {
            $result .= ' ... NOT DONE!';
        }
        $result .= EOL;
    }

    $result .= getEndingNotes($writers);

    return $result;
}

/**
 * Get ending notes
 *
 * @param array $writers
 *
 * @return string
 */
function getEndingNotes($writers)
{
    $result = '';

    // Do not show execution time for index
    if (!IS_INDEX) {
        $result .= date('H:i:s') . " Done writing file(s)" . EOL;
        $result .= date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB" . EOL;
    }

    // Return
    if (CLI) {
        $result .= 'The results are stored in the "results" subdirectory.' . EOL;
    } else {
        if (!IS_INDEX) {
            $types = array_values($writers);
            $result .= '<p>&nbsp;</p>';
            $result .= '<p>Results: ';
            foreach ($types as $type) {
                if (!is_null($type)) {
                    $resultFile = 'results/' . SCRIPT_FILENAME . '.' . $type;
                    if (file_exists($resultFile)) {
                        $result .= "<a href='{$resultFile}' class='btn btn-primary'>{$type}</a> ";
                    }
                }
            }
            $result .= '</p>';
        }
    }

    return $result;
}

// New Word Document
echo date('H:i:s'), ' Create new PhpWord object', EOL;
$phpWord = new \PhpOffice\PhpWord\PhpWord();
$section = $phpWord->addSection();
$header = array('size' => 16, 'bold' => true);

// 2. Advanced table

$section->addTextBreak(1);
$section->addText($unidade->modelName(), $header);

$fancyTableStyleName = 'Fancy Table';
$fancyTableStyle = array('borderSize' => 6, 'borderColor' => '000000', 'cellMargin' => 80, 'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER);
$fancyTableFirstRowStyle = array('borderBottomSize' => 12, 'borderBottomColor' => '0000FF', 'bgColor' => 'FFFFFF');
$fancyTableCellStyle = array('valign' => 'center');
//$fancyTableCellBtlrStyle = array('valign' => 'center', 'textDirection' => \PhpOffice\PhpWord\Style\Cell::TEXT_DIR_BTLR);
$fancyTableFontStyle = array('bold' => true);
$phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle, $fancyTableFirstRowStyle);
$table = $section->addTable($fancyTableStyleName);
$table->addRow(900);
$table->addCell(1000, $fancyTableCellStyle)->addText('Descrição', $fancyTableFontStyle);
$table->addCell(1000, $fancyTableCellStyle)->addText('Valor', $fancyTableFontStyle);
$table->addCell(1000, $fancyTableCellStyle)->addText('Vencimento', $fancyTableFontStyle);
$table->addCell(1000, $fancyTableCellStyle)->addText('Multa', $fancyTableFontStyle);
$table->addCell(1000, $fancyTableCellStyle)->addText('Atualização', $fancyTableFontStyle);
$table->addCell(1000, $fancyTableCellStyle)->addText('Valor Atualizado', $fancyTableFontStyle);
$table->addCell(1000, $fancyTableCellStyle)->addText('Total de Juros', $fancyTableFontStyle);
$table->addCell(1000, $fancyTableCellStyle)->addText('Total do Débito', $fancyTableFontStyle);
//$table->addCell(500, $fancyTableCellBtlrStyle)->addText('Row 5', $fancyTableFontStyle);
$formater = Yii::$app->formatter;
foreach ($debitos as $debito) {
    $table->addRow();
    $table->addCell(1000)->addText($debito->descricao);
    $table->addCell(1000)->addText($formater->asCurrency($debito->valorOriginal));
    $table->addCell(1000)->addText($formater->asDate($debito->vencimento));
    $table->addCell(1000)->addText($formater->asCurrency($debito->valorMulta));
    $table->addCell(1000)->addText($formater->asDecimal($debito->atualizacao->fator, 8));
    $table->addCell(1000)->addText($formater->asCurrency($debito->valorAtualizado));
    $table->addCell(1000)->addText($debito->getMesesVencidos() . '% - ' . $formater->asCurrency($debito->totalJuros));
    $table->addCell(1000)->addText($formater->asCurrency($debito->totalDebito));
}
//Soma total
$table->addRow();
$table->addCell(1000)->addText("Soma Total Débitos:", $fancyTableFontStyle);
$totalDebitos = $unidade->getTotalDebitos();
$table->addCell(1000)->addText($formater->asCurrency($totalDebitos), $fancyTableFontStyle);

//Honorários
$table->addRow();
$table->addCell(1000)->addText("Honorários:", $fancyTableFontStyle);
$honorarios = $unidade->getHonorarios();
$table->addCell(1000)->addText($formater->asCurrency($honorarios), $fancyTableFontStyle);

//Valor devido
$table->addRow();
$table->addCell(1000)->addText("Valor Total:", $fancyTableFontStyle);
$table->addCell(1000)->addText($formater->asCurrency($totalDebitos + $honorarios), $fancyTableFontStyle);

// Save file
//    echo write($phpWord, basename(__FILE__, '.php'), $writers);
$file = "/tabelas/tabela.docx";
$filename = dirname(__FILE__) . $file;
if (file_exists($filename)) {
    unlink($filename);
}
$phpWord->save($filename);
