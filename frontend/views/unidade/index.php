<?php

use common\models\Condominio;
use common\models\Proprietario;
use common\models\Unidade;
use common\widgets\Select2;
use kartik\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UnidadeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Unidades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unidade-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::button('Novo Condomínio', ['class' => 'btn btn-info',
        'data-toggle' => "modal", 'data-target' => "#novo-condominio-modal"
    ]) ?>

    <?= Html::button('Novo Proprietário', ['class' => 'btn btn-primary',
        'data-toggle' => "modal", 'data-target' => "#novo-proprietario-modal"
    ]) ?>

    <?= Html::button('Nova Unidade', ['class' => 'btn btn-success',
        'data-toggle' => "modal", 'data-target' => "#nova-unidade-modal"
    ]) ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => [
                    'class' => 'col-xs-3',
                ],
                'label' => 'Número',
                'value' => function ($model) {
                    return $model->numero .
                    Html::tag('sup', Html::a(Html::icon('new-window'), ['view', 'id' => $model->id], ['target' => '_blank']));
                },
                'contentOptions' => [
                    'class' => 'link-popup',
                ],
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id',
                    'initValueText' => Unidade::findOne($searchModel->id),
                    'objeto' => 'unidade',
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'condominioId',
                'headerOptions' => [
                    'class' => 'col-xs-4',
                ],
                'label' => 'Condomínio',
                'value' => function ($model) {
                    return $model->condominio .
                    Html::tag('sup', Html::a(Html::icon('new-window'), ['/condominio/view', 'id' => $model->condominioId], ['target' => '_blank']));
                },
                'contentOptions' => [
                    'class' => 'link-popup',
                ],
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'condominioId',
                    'initValueText' => Condominio::findOne($searchModel->condominioId),
                    'objeto' => 'condominio',
                ]),
                'format' => 'raw',
            ],
            [
                'label' => 'Proprietário',
                'attribute' => 'proprietarioId',
                'headerOptions' => [
                    'class' => 'col-xs-5',
                ],
                'value' => function ($model) {
                    return $model->proprietario ? $model->proprietario .
                        Html::tag('sup', Html::a(Html::icon('new-window'), ['/proprietario/view', 'id' => $model->proprietarioId], ['target' => '_blank'])) : '';
                },
                'contentOptions' => [
                    'class' => 'link-popup',
                ],
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'proprietarioId',
                    'initValueText' => Proprietario::findOne($searchModel->proprietarioId),
                    'objeto' => 'proprietario',
                ]),
                'format' => 'raw',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
</div>
<?= $this->render('_modalNovoCondominio'); ?>
<?= $this->render('_modalNovaUnidade'); ?>
<?= $this->render('_modalNovoProprietario'); ?>
