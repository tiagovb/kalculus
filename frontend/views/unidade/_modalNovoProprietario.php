<?php
use common\models\Proprietario;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;

Modal::begin([
    'header' => '<span id="novo-proprietario-modal-title">Novo Proprieário</span>',
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'novo-proprietario-modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]); ?>
    <div id="novo-proprietario-modal-content">
        <div class="proprietario-form">

            <?php $proprietario = new Proprietario();
            $form = ActiveForm::begin(['action' => '/unidade/novo-proprietario']); ?>

            <?= $form->field($proprietario, 'nome')->textInput(['maxlength' => true]) ?>

            <?= $form->field($proprietario, 'telefone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($proprietario, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($proprietario, 'cpf')->textInput(['maxlength' => true]) ?>


            <div class="form-group">
                <?= Html::submitButton('Cadastrar', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
<?php Modal::end(); ?>