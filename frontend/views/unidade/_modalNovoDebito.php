<?php
use common\models\Debito;
use kartik\helpers\Html;
use kartik\money\MaskMoney;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $unidadeId common\models\Debito */

$debito = new Debito(['unidadeId' => $unidadeId]);
Modal::begin([
    'header' => '<span id="novo-debito-modal-title">Novo Débito</span>',
    'id' => 'novo-debito-modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]); ?>
    <div id="novo-debito-modal-content">
        <?php $form = ActiveForm::begin(['action' => 'novo-debito?id=' . $unidadeId]); ?>

        <?= $form->field($debito, 'descricao')->textInput(['maxlength' => true]) ?>

        <?= $form->field($debito, 'vencimento')->textInput(['type' => 'date']) ?>

        <?= $form->field($debito, 'valorOriginal')->widget(MaskMoney::className(), [
            'pluginOptions' => [
                'prefix' => 'R$',
                'allowNegative' => false,
                'thousands' => '.',
                'decimal' => ',',
                'precision' => 2,
            ],
        ]) ?>
        <div class="form-group">
            <?= Html::submitButton('Calcular', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
<?php Modal::end(); ?>