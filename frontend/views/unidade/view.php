<?php

use common\models\Debito;
use kartik\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Unidade */
/* @var $debitosSearchModel common\models\search\DebitoSearch */
/* @var $debitosProvider yii\data\ActiveDataProvider */

$proprietario = $model->proprietario;
?>
<div class="unidade-view">

    <div class="row">
        <div class="col-xs-12">
            <h3>Unidade: <?= $model->numero ?></h3>
            <h4>Condomínio: <?= $model->condominio ?></h4>

            <?php if ($proprietario) : ?>
                <h4>Proprietário:
                    <?= $proprietario ?>
                    <?= $proprietario->telefone ? "/ $proprietario->telefone" : '' ?>
                    <?= $proprietario->email ? "/ $proprietario->email" : '' ?></h4>
            <?php endif; ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-6">
            <?= Html::button('Novo Débito', ['class' => 'btn btn-primary',
                'data-toggle' => "modal", 'data-target' => "#novo-debito-modal"
            ]) ?>
            <?php if (!empty($model->debitos)): ?>
                <?= Html::a('Atualizar Débitos', ['atualizar-debitos', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
                <?= Html::a('Limpar Débitos', ['limpar-debitos', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data-confirm' => 'ATENÇÃO!! Confirmar exclusão de todos os débitos desta unidade??',
                ]) ?>
                <?= Html::a('<i class="fa fa-file-word-o" aria-hidden="true"></i>Baixar Arq. Word', ['mostrar-tabela', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
            <?php endif; ?>
        </div>
        <div class="col-xs-6 text-right">
            <span>Total Débitos: <?= Yii::$app->formatter->asCurrency($model->getTotalDebitos()) ?></span><br>
            <span>Honorários: <?= Yii::$app->formatter->asCurrency($model->getHonorarios()) ?></span><br>
            <strong>Total
                Devido: <?= Yii::$app->formatter->asCurrency($model->getTotalDebitos() + $model->getHonorarios()) ?></strong>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?= GridView::widget([
                'dataProvider' => $debitosProvider,
                'filterModel' => $debitosSearchModel,
                'layout' => "{items}\n{pager}",
                'tableOptions' => ['class' => 'table table-striped table-bordered small'],
                'columns' => [
                    'descricao',
                    'valorOriginal:currency',
                    'vencimento:date',
                    'valorMulta:currency',
                    [
                        'attribute' => 'atualizacao',
                        'value' => function ($model) {
                            return str_replace('.', ',', $model->atualizacao->fator);
                        },
                    ],
                    'valorAtualizado:currency',
                    [
                        'attribute' => 'totalJuros',
                        'value' => function ($model) {
                            /** @var Debito $model */
                            return $model->getMesesVencidos() . '% - ' . Yii::$app->formatter->asCurrency($model->totalJuros);
                        },
                    ],
                    'dataCalculo:dateTime',
                    'totalDebito:currency',

                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['/debito/delete', 'id' => $model->id], [
                                    'title' => "Excluir", 'aria-label' => "Excluir", 'data-confirm' => "Confirma a exclusão deste item?", 'data-method' => "post",
                                ]);
                            }
                        ]
                    ],
                ],
            ]); ?></div>
    </div>
</div>
<?= $this->render('_modalNovoDebito', ['unidadeId' => $model->id]); ?>

