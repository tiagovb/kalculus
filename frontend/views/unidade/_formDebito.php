<?php

use kartik\money\MaskMoney;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $debito common\models\Debito */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="unidade-form">

    <?php $form = ActiveForm::begin(['action' => 'novo-debito?id=' . $debito->unidadeId]); ?>

    <?= $form->field($debito, 'descricao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($debito, 'vencimento')->textInput(['type' => 'date']) ?>

    <?= $form->field($debito, 'valorOriginal')->widget(MaskMoney::className(), [
        'pluginOptions' => [
            'prefix' => 'R$',
            'allowNegative' => false,
            'thousands' => '.',
            'decimal' => ',',
            'precision' => 2,
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Calcular', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
