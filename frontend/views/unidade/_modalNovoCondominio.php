<?php
use common\models\Condominio;
use kartik\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\bootstrap\Modal;

Modal::begin([
    'header' => '<span id="novo-condominio-modal-title">Novo Condomínio</span>',
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'novo-condominio-modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]); ?>
    <div id="novo-condominio-modal-content">
        <div class="condominio-form">

            <?php $condomino = new Condominio();
            $form = ActiveForm::begin(['action' => '/unidade/novo-condominio']); ?>

            <?= $form->field($condomino, 'razaoSocial')->textInput(['maxlength' => true]) ?>

            <?= $form->field($condomino, 'cnpj')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton('Cadastrar', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
<?php Modal::end(); ?>