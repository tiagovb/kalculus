<?php

use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Unidade */
/* @var $debitos common\models\Debito[] */
$formatter = Yii::$app->formatter;
$css = <<<CSS
.tabelaMostra{

    border: 1px;
 	width:auto; 
    margin-left:auto; 
    margin-right:auto;
	font: 10pt;
}

.tabelaMostra tr td {
    border: 1px solid #666;

}

.tabelaMostra th {
    border: 1px solid #666;
}
CSS;

$js = <<<JS
	var selectElementContents = function (el) {
    var body = document.body, range, sel;
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents(el);
            sel.addRange(range);
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
        }
    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
    }
}
JS;


$this->registerCss($css);
$this->registerJs($js);


?>
<div class="tabela-debitos">
    <div class="row">
        <div class="col-xs-12">
            <?php /*
            <?= Html::button('<span class="glyphicon glyphicon-copy"></span> ' . 'Copiar Tabela', [
                'class' => 'btn btn-info',
                'onclick' => "selectElementContents($('.tabelaMostra') );",
            ])
            */ ?>

            <?= Html::a('Voltar', ['/unidade/view', 'id' => $model->id], [
                'class' => 'btn btn-default',
            ]) ?>
        </div>
        <hr>
        <div class="col-xs-12" id="tabela">
            <table class="tabelaMostra">

                <tbody>
                <tr>
                    <th>Descrição</th>
                    <th>Valor</th>
                    <th>Vencimento</th>
                    <th>Multa</th>
                    <th>Atualização</th>
                    <th>Valor Atualizado</th>

                    <th>Total de Juros</th>
                    <th>Total do Débito</th>
                </tr>

                <?php foreach ($debitos as $debito) : ?>
                    <tr>
                        <td><?= $debito->descricao ?></td>
                        <td><?= $formatter->asCurrency($debito->valorOriginal) ?></td>
                        <td><?= $formatter->asDate($debito->vencimento) ?></td>
                        <td><?= $formatter->asCurrency($debito->valorMulta) ?></td>
                        <td><?= str_replace('.', ',', $debito->atualizacao->fator) ?></td>
                        <td><?= $formatter->asCurrency($debito->valorAtualizado) ?></td>

                        <td><?= $debito->getMesesVencidos() . '% - ' . $formatter->asCurrency($debito->totalJuros) ?></td>
                        <td class="text-right"><?= $formatter->asCurrency($debito->totalDebito) ?></td>
                    </tr>
                <?php endforeach; ?>

                <tr>
                    <th>Soma dos Débitos:</th>
                    <th><?= $formatter->asCurrency($model->getTotalDebitos()) ?></th>
                </tr>
                <tr>
                    <th>Honorários:</th>
                    <th><?= $formatter->asCurrency($model->getHonorarios()) ?></th>
                </tr>
                <tr>
                    <th>Soma Total:</th>
                    <th><?= $formatter->asCurrency((float)($model->getHonorarios()) + (float)($model->getTotalDebitos())) ?></th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
