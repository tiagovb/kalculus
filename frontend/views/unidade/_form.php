<?php

use common\models\Condominio;
use common\models\Proprietario;
use common\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Unidade */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unidade-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numero')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'condominioId')->widget(Select2::className(), [
        'model' => $model,
        'initValueText' => Condominio::findOne($model->condominioId),
        'attribute' => 'condominioId',
        'objeto' => 'condominio',
    ]) ?>

    <?= $form->field($model, 'proprietarioId')->widget(Select2::className(), [
        'model' => $model,
        'initValueText' => Proprietario::findOne($model->proprietarioId),
        'attribute' => 'proprietarioId',
        'objeto' => 'proprietario',
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Cadastrar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
