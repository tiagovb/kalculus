<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Atualizacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="atualizacao-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fator')->textInput() ?>

    <?= $form->field($model, 'dtPublicacao')->textInput() ?>

    <?= $form->field($model, 'mesReferencia')->textInput() ?>

    <?= $form->field($model, 'anoReferencia')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
