<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AtualizacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Atualizacões';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atualizacao-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fator',
            [
                'attribute' => 'dtPublicacao',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->dtPublicacao, 'php:d/m/Y');
                },
            ],
            'mesReferencia',
            'anoReferencia',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
