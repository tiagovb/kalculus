<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Atualizacao */

$this->title = 'Atualizar tabela';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atualizacao-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <a href="http://www.tjmg.jus.br/portal-tjmg/processos/indicadores/fator-de-atualizacao-monetaria.htm" target="IFrame">Ver Atualizações</a>
    <div class="login-box-body">

        <?= Html::beginForm() ?>
        <div class="form-group required">
            <?= Html::label('Endereço da tabela (Arquivo PDF)', 'url-tabela', ['class' => 'control-label']) ?>
            <?= Html::textInput('url-tabela', null, ['class' => 'form-control']) ?>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat" name="login-button">Continuar</button>
            </div>
        </div>
        <?= Html::endForm() ?>
    </div>

</div>
