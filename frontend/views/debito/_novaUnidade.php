<?php

use common\models\Condominio;
use common\models\Proprietario;
use common\models\Unidade;
use kartik\helpers\Html;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
$model = new Unidade();
$condominios = ArrayHelper::map(Condominio::find()->asArray()->all(), 'id', 'razaoSocial');
$proprietarios = ArrayHelper::map(Proprietario::find()->asArray()->all(), 'id', 'nome');
?>

<div id="nova-unidade" style="display: none;">
    <div class="unidade-form">

        <?php $form = ActiveForm::begin([
            'action' => ['/unidade/create']
        ]); ?>

        <?= $form->field($model, 'numero')->textInput(['maxlength' => true]) ?>

        <?=
        $form->field($model, 'condominioId')->widget(Select2::className(),
            [
                'model' => $model,
                'attribute' => 'condominioId',
                'data' => $condominios,
                'options' => ['placeholder' => 'Selecionar Condomínio'],
                'addon' => [
                    'append' => [
                        'content' => Html::button(Html::icon('plus'), [
                            'id' => 'btn-novo-condomio',
                            'class' => 'btn btn-default',
                            'title' => 'Criar novo',
                            'data-toggle' => 'tooltip',
                            'target' => '_blank',
                        ]),
                        'asButton' => true
                    ]
                ]
            ]) ?>

        <?= $form->field($model, 'proprietarioId')->widget(Select2::className(),
            [
                'model' => $model,
                'attribute' => 'proprietarioId',
                'data' => $proprietarios,
                'options' => ['placeholder' => 'Selecionar Proprietario'],
                'addon' => [
                    'append' => [
                        'content' => Html::button(Html::icon('plus'), [
                            'id' => 'btn-novo-proprietario',
                            'class' => 'btn btn-default',
                            'title' => 'Criar novo',
                            'data-toggle' => 'tooltip',
                        ]),
                        'asButton' => true
                    ]
                ]
            ]) ?>

        <?= Html::button('Canelar', ['onclick' => '$(\'#nova-unidade\').slideUp(300);']) ?>
        <?= Html::submitButton(Yii::t('app', 'Salvar'), ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end(); ?>

    </div>
</div>

