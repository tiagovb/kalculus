<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Debito */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="debito-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'vencimento')->textInput() ?>

    <?= $form->field($model, 'valorOriginal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'totalDebito')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dataCalculo')->textInput() ?>

    <?= $form->field($model, 'valorMulta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'valorAtualizado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'totalJuros')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descricao')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unidadeId')->textInput() ?>

    <?= $form->field($model, 'atualizacaoId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Cadastrar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
