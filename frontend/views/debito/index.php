<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DebitoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Debitos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="debito-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Debito', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'vencimento',
            'valorOriginal',
            'totalDebito',
            'dataCalculo',
            // 'valorMulta',
            // 'valorAtualizado',
            // 'totalJuros',
            // 'descricao',
            // 'unidadeId',
            // 'atualizacaoId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
