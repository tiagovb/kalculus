<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Debito */

$this->title = 'Update Debito: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Debitos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="debito-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
