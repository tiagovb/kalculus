<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\DebitoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="debito-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vencimento') ?>

    <?= $form->field($model, 'valorOriginal') ?>

    <?= $form->field($model, 'totalDebito') ?>

    <?= $form->field($model, 'dataCalculo') ?>

    <?php // echo $form->field($model, 'valorMulta') ?>

    <?php // echo $form->field($model, 'valorAtualizado') ?>

    <?php // echo $form->field($model, 'totalJuros') ?>

    <?php // echo $form->field($model, 'descricao') ?>

    <?php // echo $form->field($model, 'unidadeId') ?>

    <?php // echo $form->field($model, 'atualizacaoId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
