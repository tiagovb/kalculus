<?php

use common\models\Proprietario;
use common\widgets\Select2;
use kartik\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProprietarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proprietarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proprietario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Proprietario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'label' => 'Nome',
                'attribute' => 'id',
                'headerOptions' => [
                    'class' => 'col-xs-5',
                ],
                'value' => function ($model) {
                    return $model->nome .
                    Html::tag('sup', Html::a(Html::icon('new-window'), ['view', 'id' => $model->id], ['target' => '_blank']));
                },
                'contentOptions' => [
                    'class' => 'link-popup',
                ],
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'id',
                    'initValueText' => Proprietario::findOne($searchModel->id),
                    'objeto' => 'proprietario',
                ]),
                'format' => 'raw',
            ],
            'telefone',
            'email',
            'cpf',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
