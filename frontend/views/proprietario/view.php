<?php

use common\models\Condominio;
use common\models\Unidade;
use common\widgets\Select2;
use kartik\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Proprietario */
/* @var $unidadeSearchModel common\models\search\UnidadeSearch */
/* @var $unidadeProvider yii\data\ActiveDataProvider */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Proprietarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proprietario-view">

    <h1><?= $model->modelName() ?></h1>
    <h4><?= "Telefone: $model->telefone" ?></h4>
    <h4><?= "Email: $model->email" ?></h4>
    <h4><?= "CPF: $model->cpf" ?></h4>

    <p>        <?= Html::a('Alterar Proprietário', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <h3>Unidades deste proprietário:</h3>

    <?= GridView::widget([
        'dataProvider' => $unidadeProvider,
        'filterModel' => $unidadeSearchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => [
                    'class' => 'col-xs-3',
                ],
                'label' => 'Número',
                'value' => function ($model) {
                    return $model->numero .
                    Html::tag('sup', Html::a(Html::icon('new-window'), ['view', 'id' => $model->id], ['target' => '_blank']));
                },
                'contentOptions' => [
                    'class' => 'link-popup',
                ],
                'filter' => Select2::widget([
                    'model' => $unidadeSearchModel,
                    'attribute' => 'id',
                    'initValueText' => Unidade::findOne($unidadeSearchModel->numero),
                    'objeto' => 'unidade',
                ]),
                'format' => 'raw',
            ],
            [
                'attribute' => 'condominioId',
                'headerOptions' => [
                    'class' => 'col-xs-4',
                ],
                'label' => 'Condomínio',
                'value' => function ($model) {
                    return $model->condominio .
                    Html::tag('sup', Html::a(Html::icon('new-window'), ['/condominio/view', 'id' => $model->condominioId], ['target' => '_blank']));
                },
                'contentOptions' => [
                    'class' => 'link-popup',
                ],
                'filter' => Select2::widget([
                    'model' => $unidadeSearchModel,
                    'attribute' => 'condominioId',
                    'initValueText' => Condominio::findOne($unidadeSearchModel->condominioId),
                    'objeto' => 'condominio',
                ]),
                'format' => 'raw',
            ],
            'totalDebitos:currency',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
