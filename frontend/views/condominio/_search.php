<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\CondominioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="condominio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'razaoSocial') ?>

    <?= $form->field($model, 'cnpj') ?>

    <?= $form->field($model, 'endLogradouro') ?>

    <?= $form->field($model, 'endNumero') ?>

    <?php // echo $form->field($model, 'endComplemento') ?>

    <?php // echo $form->field($model, 'endBairro') ?>

    <?php // echo $form->field($model, 'endCidade') ?>

    <?php // echo $form->field($model, 'endEstado') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
