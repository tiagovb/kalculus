<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Condominio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="condominio-form">

    <?php $form = ActiveForm::begin(['action' => '/condominio/create']); ?>

    <?= $form->field($model, 'razaoSocial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cnpj')->textInput(['maxlength' => true]) ?>

    <?php /*
    <?= $form->field($model, 'endLogradouro')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endNumero')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endComplemento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endBairro')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endCidade')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endEstado')->textInput(['maxlength' => true]) ?>
   */ ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Cadastrar' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
