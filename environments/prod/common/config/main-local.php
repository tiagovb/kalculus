<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=kalculus',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'talma\sendgrid\Mailer',
            'key' => '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',
            'viewPath' => '@common/mail',
        ],
    ],
];
