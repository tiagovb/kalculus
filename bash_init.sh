#!/bin/bash

run mysql --user=root --password=vagrant -e "DROP DATABASE kalculus; CREATE DATABASE kalculus;"

run php /app/init

run php /app/yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations --interactive=0

run php /app/yii migrate/up --interactive=0

printf "\n\n%s\n\n" "${green}Complete!${NC}\n"
