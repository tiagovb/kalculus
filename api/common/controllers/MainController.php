<?php

namespace app\common\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\Action;
use yii\rest\ActiveController;

/**
 * Class MainController
 */
class MainController extends ActiveController
{
    /**
     * @inheritDoc
     */
    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = 'app\common\controllers\MainController::prepareDataProvider';

        return $actions;
    }

    /**
     * @param $action
     *
     * @return ActiveDataProvider
     */
    public static function prepareDataProvider(Action $action)
    {
        /* @var $modelClass \yii\db\BaseActiveRecord */
        $modelClass = $action->modelClass;

        return new ActiveDataProvider([
            'query' => $modelClass::find(),
            'pagination' => [
                'pageSizeLimit' => 1000,
                'defaultPageSize' => 500
            ]
        ]);
    }
}
