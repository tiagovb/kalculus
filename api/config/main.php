<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'api',
    'modules' => [
        'v1' => [
            'class' => 'app\modules\v1\RestModule'
        ]
    ],
    'components' => [
        'user' => [
            'enableSession' => false
        ],
        'response' => [
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                    // ...
                ],
            ],
            'charset' => 'UTF-8',
            'on beforeSend' => function ($event) {
                $event->sender->getHeaders()->set("ML-Timestamp", time());
            }
        ],
        'request' => [
            'class' => '\yii\web\Request',
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'urlManager' => [
            'enableStrictParsing' => true,
            'rules' => [
                'GET /notas' => 'v1/academico/notas',
            ],
        ],
    ],
    'params' => $params,
];
