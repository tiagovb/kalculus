<?php
namespace api\modules\v1\controllers;

use common\helpers\NotasHelper;
use Yii;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Site controller
 */
class AcademicoController extends Controller
{
    /**
     * @return mixed
     */
    public function actionNotas()
    {
        $navegarNotas = NotasHelper::navegarNotas();

        return $navegarNotas;
    }
}
